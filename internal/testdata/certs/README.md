# Certificates used to test HTTPS connections

Generated certificates as part of https://gitlab.com/gitlab-org/release-cli/-/merge_requests/86.
Followed [this guide](https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/)
to generate all required files.

## Use makefile to generate certificates

You can generate all keys, CSRs, CA and self-signed certificate in one go by running:

```shell
$ make generate_certs

. ./internal/testdata/certs/generate_certs.sh && gen_all
Generating RSA key: $PWD/release-cli/internal/testdata/certs/CA.key
Generating RSA private key, 2048 bit long modulus
.........+++
...........+++
e is 65537 (0x10001)
Generating CA certificate: $PWD/release-cli/internal/testdata/certs/CA.pem
Generating RSA key: $PWD/release-cli/internal/testdata/certs/localhost.key
Generating RSA private key, 2048 bit long modulus
................................+++
..+++
e is 65537 (0x10001)
Generating certificate signing request (CSR) for: $PWD/release-cli/internal/testdata/certs/localhost.csr
Generating self-signed-certificate: $PWD/release-cli/internal/testdata/certs/localhost.pem
Signature ok
subject=/C=US/ST=CA/L=San Francisco/O=GitLab/OU=Release/CN=127.0.0.1/emailAddress=support@gitlab.com
Getting CA Private Key
```

## Generate Certificate Authority

You need to generate a Certificate Authority first. You will need
a `.key` file before you can generate a `.pem`.

1. Generate a `CA.key` file

    ```shell
    $ openssl genrsa -out CA.key 2048
    Generating RSA private key, 2048 bit long modulus
    ...................+++
    ..............................+++
    e is 65537 (0x10001)
    ```

1. Generate a root certificate, only fill the fields shown below 

   ```shell
   $ openssl req -x509 -new -nodes -key CA.key -sha256 -out CA.pem                                                                   ─╯
   You are about to be asked to enter information that will be incorporated
   into your certificate request.
   What you are about to enter is what is called a Distinguished Name or a DN.
   There are quite a few fields but you can leave some blank
   For some fields there will be a default value,
   If you enter '.', the field will be left blank.
   -----
   Country Name (2 letter code) []:
   State or Province Name (full name) []:
   Locality Name (eg, city) []:
   Organization Name (eg, company) []:GitLab
   Organizational Unit Name (eg, section) []:
   Common Name (eg, fully qualified host name) []:127.0.0.1
   Email Address []:support@gitlab.com
   ```

1. You should now have two files: `CA.key` and `CA.pem`.

## Generate a self-signed certificate

Now we need a certificate for `127.0.0.1` along with a Certificate Signing Request (CSR).

1. Generate a key for your certificate

   ```shell
   $ openssl genrsa -out localhost.key 2048
   Generating RSA private key, 2048 bit long modulus
   ....................+++
   ......................................................................................................................+++
   e is 65537 (0x10001)
   ```

1. Generate a CSR, fill in the following details (leave password blank)

   ```shell
   $ openssl req -new -key localhost.key -out localhost.csr

   You are about to be asked to enter information that will be incorporated
   into your certificate request.
   What you are about to enter is what is called a Distinguished Name or a DN.
   There are quite a few fields but you can leave some blank
   For some fields there will be a default value,
   If you enter '.', the field will be left blank.
   -----
   Country Name (2 letter code) []:
   State or Province Name (full name) []:
   Locality Name (eg, city) []:
   Organization Name (eg, company) []:GitLab
   Organizational Unit Name (eg, section) []:
   Common Name (eg, fully qualified host name) []:127.0.0.1
   Email Address []:support@gitlab.com
   
   Please enter the following 'extra' attributes
   to be sent with your certificate request
   A challenge password []:
   ```

1. We also need a special `-extfile`, 
for more details see [this answer](https://serverfault.com/questions/611120/failed-tls-handshake-does-not-contain-any-ip-sans).

1. Create file called `v3.ext` and add the following content

   ```plaintext
   authorityKeyIdentifier=keyid,issuer
   basicConstraints=CA:FALSE
   keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
   subjectAltName = IP:127.0.0.1
   ```

1. Now we need to generate the self-signed certificate `localhost.pem` using our custom CA!

   ```shell
   $ openssl x509 -req -in localhost.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out localhost.pem -sha256 -extfile v3.ext
   Signature ok
   subject=/O=GitLab/CN=127.0.0.1/emailAddress=support@gitlab.com
   Getting CA Private Key
   ```
